#
# C declaration composer for GCC Lua plugin.
# Copyright © 2013–2015 Peter Colberg.
# Distributed under the MIT license. (See accompanying file LICENSE.)
#

PREFIX = /usr/local
LUADIR = $(PREFIX)/share/lua/5.1
INCDIR = $(PREFIX)/include

INSTALL_D = install -d
INSTALL_F = install -m 644

FILES_LUA = cdecl.lua
FILES_FFICDECL_LUA = ffi-cdecl.lua
FILES_FFICDECL_INC = ffi-cdecl.h
FILES_FFICDECL_DOC = C.c C.lua.in Makefile

all: test

install:
	$(INSTALL_D) $(DESTDIR)$(LUADIR)/gcc
	cd gcc && $(INSTALL_F) $(FILES_LUA) $(DESTDIR)$(LUADIR)/gcc
	$(INSTALL_D) $(DESTDIR)$(LUADIR)
	cd ffi-cdecl && $(INSTALL_F) $(FILES_FFICDECL_LUA) $(DESTDIR)$(LUADIR)
	$(INSTALL_D) $(DESTDIR)$(INCDIR)
	cd ffi-cdecl && $(INSTALL_F) $(FILES_FFICDECL_INC) $(DESTDIR)$(INCDIR)

clean:

SUBDIRS = test

.PHONY: $(SUBDIRS)

$(SUBDIRS):
	@$(MAKE) -C $@
