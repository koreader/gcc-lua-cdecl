---
title: C declaration composer for the GNU Compiler Collection
pagetitle: cdecl for GCC
git: https://git.colberg.org/peter/gcc-lua-cdecl
menu:
  - doc/install.md
  - doc/manual.md
  - doc/ffi-cdecl.md
  - doc/reference.md
  - NEWS.md
  - LICENSE.md
---

![](doc/gcc-lua-cdecl.svg "cdecl for GCC"){.right width=128 height=128}
[cdecl for GCC] is a Lua module that composes C declarations from a C source
file using the [Lua plugin for GCC]. The module generates C99 function,
variable and type declarations, and supports the GCC extensions for
[attributes][GCC attribute] and [vector types][GCC vector]. The module may
be used to generate C library bindings for a foreign function interface,
e.g., [LuaJIT FFI]. The module is named after [cdecl], a program which converts
C declarations from a human-readable phrase to C code, and vice versa.

[cdecl]: https://packages.debian.org/stable/cdecl
[cdecl for GCC]: https://peter.colberg.org/gcc-lua-cdecl
[GCC attribute]: https://gcc.gnu.org/onlinedocs/gcc/Attribute-Syntax.html
[GCC vector]: https://gcc.gnu.org/onlinedocs/gcc/Vector-Extensions.html
[LuaJIT FFI]: https://luajit.org/ext_ffi.html
[Lua plugin for GCC]: https://peter.colberg.org/gcc-lua
